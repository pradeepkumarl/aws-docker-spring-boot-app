package com.classpath.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersApiAwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersApiAwsApplication.class, args);
	}

}
