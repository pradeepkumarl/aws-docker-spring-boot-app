package com.classpath.orders.controller;

import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.User;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
	
	@GetMapping
	public Set<User> fetchAll(){
		return Set.of(
				User.builder().email("rahul@gmail.com").name("rahul").id(11).build(),
				User.builder().email("vinay@gmail.com").name("vinay").id(12).build(),
				User.builder().email("harish@gmail.com").name("harish").id(13).build(),
				User.builder().email("akash@gmail.com").name("akash").id(14).build()
				);
	}

}
